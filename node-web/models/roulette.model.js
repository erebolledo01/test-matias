module.exports = mongoose => {
  const PrizeTableSchema = new mongoose.Schema({
    prize: String,
    probability: Number
  });

  var schema = mongoose.Schema(
    {
      name: String,
      cost: Number,
      prize_table: [PrizeTableSchema]
    },
    { timestamps: true }
  );

  schema.method("toJSON", function() {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  const Roulette = mongoose.model("roulette", schema);

  return Roulette;
};
