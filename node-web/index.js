const express = require('express');
const cors = require("cors");
const pug = require('pug');

var corsOptions = {
  origin: "http://localhost:5001"
};

// const MongoClient = require('mongodb').MongoClient;

const app = express();
app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
// app.engine('pug', require('pug').__express)
app.set('view engine', 'pug');

// Conexion a bd
const db = require("./models");

db.mongoose.connect(db.url, {
  useNewUrlParser: true,
  useUnifiedTopology: true
})
.then(() => {
  console.log("Connected to the database!");
})
.catch(err => {
  console.log("Cannot connect to the database!", err);
  process.exit();
});

// Ruta principal 
app.get('/', (req, res) => {
  res.render('index.pug', {title: 'La Tombola'});
});

// Rutas
require("./routes/roulette.routes")(app);
require("./routes/player.routes")(app);

app.listen(5000, () => { 
  console.log('Express is listening on port 5000!')
})