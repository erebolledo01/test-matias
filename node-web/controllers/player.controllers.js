const db = require("../models");
const Player = db.players;

// Crea/actualiza un usuario
exports.create = (req, res) => {
  let action = req.body.action

  const player = new Player({
    token: req.body.token,
    credit: 1,
    last_refresh: new Date()
  });

  // Busca el jugador en la bd, si exoste lo actualiza, sino lo crea
  Player.findOne({token: player.token})
    .then(data => {
      if(!data) {
        player.save(player)
          .then(data => {
            res.json({ message: "Jugador creado", data: data });
          })
      }else{
        let newPlayer = {}

        if(action=='refresh') {
          let now = (new Date()).getTime()
          let last_refresh = (new Date(data.last_refresh)).getTime()

          let diff = now - last_refresh

          if(diff>59000) {
            newPlayer.credit = data.credit + 1
            newPlayer.last_refresh = new Date()
          }
        }

        if(action=='play') {
          newPlayer.credit = data.credit - req.body.cost
          data.prizes.push(req.body.prize)
          newPlayer.prizes = data.prizes
        }        

        Player.findOneAndUpdate({token: player.token}, {$set: newPlayer}, {new: true})
          .then(dataPlayer => {
            res.json({ message: "Jugador actualizado", data: dataPlayer });
          })
      }
    })
};