const db = require("../models");
const Roulette = db.roulettes;

// Crea una ruleta
exports.create = (req, res) => {
  const roulette = new Roulette({
    name: req.body.name,
    cost: req.body.cost,
    prize_table: req.body.prize_table, 
  });

  // Guardar Ruleta en la bd
  roulette.save(roulette)
    .then(data => {
      res.json({ message: "Ruleta creada", data: data });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Ocurrio un error creando la Ruleta."
      });
    });
};

// Devuelve todas las ruletas
exports.findAll = (req, res) => {
  Roulette.find({})
    .then(data => {
      res.json({ message: "Ruletas", data: data });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Ocurrio un error devolviendo las Ruletas."
      });
    });
};

// Devuelve el resultado de girar una ruleta
exports.spin = async (req, res) => {
  const id = req.params.id;
  let roulette = await Roulette.findById(id).exec()

  let prizeTable = roulette.prize_table

  var weights = prizeTable.map(item=>item.probability/100)
  var results = prizeTable.map(item=>item.prize)

  let result = function() {
    var num = Math.random(),
      s = 0,
      lastIndex = weights.length - 1;

    for (var i = 0; i < lastIndex; ++i) {
      s += weights[i];
      if (num < s) {
        return results[i];
      }
    }

    return results[lastIndex];
  };

  let resultPrize = result()

  res.json({ result: resultPrize });
}
