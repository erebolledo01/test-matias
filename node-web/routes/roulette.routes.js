module.exports = app => {
  const roulettes = require("../controllers/roulette.controllers");

  var router = require("express").Router();

  // Crear una Ruleta
  router.post("/", roulettes.create);

  // Listar las ruletas
  router.get("/", roulettes.findAll);

  // Girar ruleta
  router.get("/spin/:id", roulettes.spin);

  app.use("/api/roulette", router);
}