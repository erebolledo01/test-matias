module.exports = app => {
  const users = require("../controllers/player.controllers");

  var router = require("express").Router();

  // Crear un Usuario
  router.post("/", users.create);

  app.use("/api/player", router);
}